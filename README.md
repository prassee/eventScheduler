### EventScheduler 

**Using the project** 

*(assuming Scala,SBT are all installed)*

**Running**

open the SBT prompt 

`runMain service.SchedulerApp`

the service starts running @ port 9090
a in-memory instance of H2 is being used. 

**Scheduling**

Two types of events couldbe scheduled 

* One Time Event
* Frequently running Event