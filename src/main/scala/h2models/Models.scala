package h2models

import java.sql.Timestamp

import slick.driver.H2Driver.api._

/**
  * mapping tables between slick and h2
  */
trait EventSchedule

case class OtEventSchedule(eventId: Option[Long], scheduleDate: Timestamp, eventType: String)
    extends EventSchedule

class OtEventScheduleTable(tag: Tag) extends Table[OtEventSchedule](tag, "one_time_event") {
  def eventId   = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def schDate   = column[Timestamp]("sch_date")
  def eventType = column[String]("event_type")
  def *         = (eventId.?, schDate, eventType) <> (OtEventSchedule.tupled, OtEventSchedule.unapply)
}

case class FreqEventSchedule(eventId: Option[Long], freq: Int, freqUnit: String, eventType: String)
    extends EventSchedule

class FreqEventScheduleTable(tag: Tag) extends Table[FreqEventSchedule](tag, "freq_event") {
  def eventId   = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def freq      = column[Int]("freq")
  def freqUnit  = column[String]("freq_unit")
  def eventType = column[String]("event_type")
  def *         = (eventId.?, freq, freqUnit, eventType) <> (FreqEventSchedule.tupled, FreqEventSchedule.unapply)
}

case class EventRunHistory(
    id: Option[Long], eventId: Long, executedOn: Timestamp, exeStatus: String, eventSchType: String)

class EventRunHistoryTable(tag: Tag) extends Table[EventRunHistory](tag, "event_history") {
  def id         = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def eventId    = column[Long]("event_id")
  def executedOn = column[Timestamp]("exec_on")
  def execStatus = column[String]("exec_status")
  def schType    = column[String]("sch_type")
  def * =
    (id.?, eventId, executedOn, execStatus, schType) <> (EventRunHistory.tupled, EventRunHistory.unapply)
}
