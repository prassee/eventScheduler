package h2models

import com.typesafe.config.ConfigFactory
import slick.driver.H2Driver.api._

import scala.concurrent.Future

/**
  * interface to hold db connection and tables
  */
trait DBConstants {
  private val config = ConfigFactory.load()
  val otEvents       = TableQuery[OtEventScheduleTable]
  val freqEvents     = TableQuery[FreqEventScheduleTable]
  val eventLogs      = TableQuery[EventRunHistoryTable]
  val db             = Database.forURL(config.getString("db.url"), driver = config.getString("db.driver"))
}

/**
  * Single monolithic Dao to interact with tables
  * since the db operations are relatively small a course level dao is used.
  */
object Dao extends DBConstants {
  def createOTEventSch(event: OtEventSchedule): Future[Long] = {
    db.run(otEvents.returning(otEvents.map(_.eventId)) += event)
  }

  def createFreqEventSch(event: FreqEventSchedule): Future[Long] = {
    db.run(freqEvents.returning(freqEvents.map(_.eventId)) += event)
  }

  def logEventRunHistory(runHistory: EventRunHistory): Future[Long] = {
    db.run(eventLogs.returning(eventLogs.map(_.id)) += runHistory)
  }

  def getEventLogs: Future[Seq[EventRunHistory]] = db.run(eventLogs.result)
}

/**
  *  utility class to create tables 
  */
object DML extends DBConstants {
  def createTables = {
    val dml = DBIO.seq((otEvents.schema ++ freqEvents.schema ++ eventLogs.schema).create)
    db.run(dml)
  }
}
