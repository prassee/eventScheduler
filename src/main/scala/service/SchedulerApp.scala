package service

import h2models.{DML, Dao}
import org.analogweb.core.Servers
import org.analogweb.scala.Analogweb
import scheduler.{FreqScheduleForm, OtScheduleForm, SchedulerService}

import scala.io.Source

/**
  * The app which exposes the REST layer to the scheduling logic
  */
object SchedulerApp extends Analogweb {

  import scala.concurrent.ExecutionContext.Implicits.global

  def main(args: Array[String]) = {

    DML.createTables

    Servers.run(9090)
  }

  post("/schedule/event/ote") { implicit r =>
    val x =
      json.as[OtScheduleForm](Source.fromInputStream(r.context.getRequestBody).getLines().mkString("")).get
    s"""{"status" : "${SchedulerService.createOtSchedule(x)}"}"""
  }

  post("/schedule/event/freq") { implicit r =>
    val x =
      json.as[FreqScheduleForm](Source.fromInputStream(r.context.getRequestBody).getLines().mkString("")).get
    s"""{"status" : "${SchedulerService.createFreqSchedule(x)}"}"""
  }

  get("/get/schedules") {
    for {
      res <- Dao.getEventLogs
    } yield res.mkString("")
  }
}
