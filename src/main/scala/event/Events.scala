package event

import event.EventExecStatus.EventExecStatus

/**
  * Top level classes for Event definition
  */
trait ExecutableEvent {
  def execute: EventExecStatus
}

class DummyEventImpl extends ExecutableEvent {
  override def execute: EventExecStatus = {
    println("Executing Event")
    EventExecStatus.Success
  }
}

object EventExecStatus extends Enumeration {
  type EventExecStatus = Value
  val Success, Failure = Value
}

import event.EventExecStatus.{EventExecStatus, _}

/**
  * some sample event Implementation further could be added
  */
class StockSymbolLookupEvent extends ExecutableEvent {
  override def execute: EventExecStatus = {
    println(s"called StockSymbolImpl ${this.getClass.getName}")
    Success
  }
}

class WeatherDataLookupEvent extends ExecutableEvent {
  override def execute: EventExecStatus = {
    println(s"called WeatherLookupEvent ${this.getClass.getName}")
    Success
  }
}

class SendAbridgedEmailEvent extends ExecutableEvent {
  override def execute: EventExecStatus = {
    println(s"called SendAbridgedEmailEvent ${this.getClass.getName}")
    Success
  }
}
