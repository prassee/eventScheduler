package scheduler

import java.sql.Timestamp
import java.util.Calendar

import h2models.{FreqEventSchedule, OtEventSchedule}

case class OtScheduleForm(date: String, clazz: String)

case class FreqScheduleForm(freq: Int, units: String, clazz: String)

object SchedulerService {

  // create a OneTimeScheduler
  def createOtSchedule(otScheduleForm: OtScheduleForm) = {
    val calendar = Calendar.getInstance()
    import DateConvImplicits._
    calendar.setTime(otScheduleForm.date.toDate())
    val runMay1 = OtEventSchedule(None, new Timestamp(calendar.getTime.getTime), otScheduleForm.clazz)
    Scheduler.createSchedule(runMay1)
  }

  // create a per minute Scheduler
  def createFreqSchedule(freqScheduleForm: FreqScheduleForm) = {
    val freqRun = FreqEventSchedule(
        None, freqScheduleForm.freq, freqScheduleForm.units, freqScheduleForm.clazz)
    Scheduler.createSchedule(freqRun)
  }
}
