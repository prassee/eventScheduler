package scheduler

import java.text.SimpleDateFormat

object DateConvImplicits {
  private val sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss aa")

  implicit class FromString(date: String) {
    def toDate() = {
      sdf.parse(date)
    }
  }
}
