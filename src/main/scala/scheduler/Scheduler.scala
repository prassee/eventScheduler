package scheduler

import java.sql.Timestamp
import java.util.{Timer, TimerTask}

import akka.actor.{Actor, ActorSystem, Props}
import event.ExecutableEvent
import h2models._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

/**
  * the main scheduler class which
  * schedules various events
  * We use
  * a) Timer Task impl for a one time event
  * b) Actor for Frequently occurring Events
  * the respective implmentation is given below
  */
object Scheduler {
  val as = ActorSystem("Scheduler")
  def createSchedule(es: EventSchedule) = {
    var status = "Scheduler Not Created"
    es match {
      case ote: OtEventSchedule =>
        Dao.createOTEventSch(ote).onComplete {
          case Success(e) =>
            println(s"ote will be scheduled at ${ote.scheduleDate}")
            val timer           = new Timer()
            val executableEvent = Class.forName(ote.eventType).newInstance().asInstanceOf[ExecutableEvent]
            timer.schedule(new ExecOnceTask(executableEvent, e), ote.scheduleDate)
            status = "Scheduler Created"
          case Failure(e) =>
        }
      case fte: FreqEventSchedule =>
        Dao.createFreqEventSch(fte).onComplete {
          case Success(e) =>
            val executableEvent = Class.forName(fte.eventType).newInstance().asInstanceOf[ExecutableEvent]
            as.actorOf(Props(classOf[EventActor], executableEvent, e, fte))
            status = "Scheduler Created"
          case Failure(e) =>
        }
    }
    status
  }
}

class ExecOnceTask(executableEvent: ExecutableEvent, eventId: Long) extends TimerTask {
  override def run(): Unit = {
    val status   = executableEvent.execute.toString
    val eventLog = EventRunHistory(None, eventId, new Timestamp(System.currentTimeMillis()), status, "")
    Dao.logEventRunHistory(eventLog)
  }
}

class EventActor(eventType: ExecutableEvent, eventId: Long, eventSchedule: EventSchedule) extends Actor {
  case object ExecuteEvent
  import scala.concurrent.duration._
  override def preStart() = {
    val scheduler = context.system.scheduler
    eventSchedule match {
      case ote: OtEventSchedule =>
      case fte: FreqEventSchedule => {
          val freq = fte.freqUnit match {
            case "minutes" => fte.freq minutes
            case "seconds" => fte.freq seconds
            case "hours"   => fte.freq hours
            case "days"    => fte.freq days
          }
          scheduler.schedule(0 second, freq, self, ExecuteEvent)
        }
    }
  }

  def receive = {
    case ExecuteEvent =>
      val status   = eventType.execute.toString
      val eventLog = EventRunHistory(None, eventId, new Timestamp(System.currentTimeMillis()), status, "")
      Dao.logEventRunHistory(eventLog)
  }
}
