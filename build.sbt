name := "eventScheduler"

version := "1.0"

scalaVersion := "2.11.7"

val akkaVersion = "2.4.2"
val akka = "com.typesafe.akka" % "akka-actor_2.11" % akkaVersion
val akkaSlf4j = "com.typesafe.akka" % "akka-slf4j_2.11" % akkaVersion
val akkaRemote = "com.typesafe.akka" %% "akka-remote" % akkaVersion
val slick = "com.typesafe.slick" %% "slick" % "3.1.1"
val jodatime = "joda-time" % "joda-time" % "2.7"
val jodaconvert = "org.joda" % "joda-convert" % "1.7"
val slickJodaMapper = "com.github.tototoshi" %% "slick-joda-mapper" % "2.1.0"
val h2 = "com.h2database" % "h2" % "1.3.176"
val analogWeb = "org.analogweb" %% "analogweb-scala" % "0.9.12"
val upickle = "com.lihaoyi" %% "upickle" % "0.3.9"

val scalaTest = "org.scalatest" % "scalatest_2.11" % "2.2.6" % "test"
val scalamock = "org.scalamock" %% "scalamock-scalatest-support" % "3.2" % "test"
val akkaTestkit = "com.typesafe.akka" % "akka-testkit_2.11" % akkaVersion % "test"
val cassunit = "org.cassandraunit" % "cassandra-unit" % "2.1.9.2" % "test"

resolvers += "Twitter" at "http://maven.twttr.com"

libraryDependencies ++= Seq(
  // compile deps
  akka, akkaSlf4j, akkaRemote, slick, jodatime, slickJodaMapper, h2, analogWeb,upickle,
  // test deps
  scalaTest, akkaTestkit)



